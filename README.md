# REGISTRO DE PROYECTO
### Posibles implementaciones
  - Menu generado con js, para las opciones de la pagina
  - Efectos animados para la aparicion de imagenes o texto
  - Generar estadisticas con los datos obtenidos

### v18-05
  - Modelo Post/User, funcionalidad create, index;
  - Falta guardar en BBDD los posts con el registro y filtrar cuando se muestren con un usuario concreto.
  
### v20/05
  - Importante incluir en la ruta proyectodaw19/core/App.php, los modelos que tiene que cargar la aplicacion antes de cargar

### v23-05
  - Falta de permisos a lo usuario
  - NO poder borrar su propio usuario
## v26-05
  - La ruta del user index, esta mal. Hay que redirigirla
  - Comprobar validacion de datos antes de enviar a store.
  - Falta validacion de correo antes de insertar datos en BBDD
