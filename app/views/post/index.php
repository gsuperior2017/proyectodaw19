<!doctype html>
<html lang="es">
  <head>
    <?php require "../app/views/parts/head.php" ?>
  </head>
    <body>
      <?php require "../app/views/parts/header.php" ?>
      <article>
      <div>
        <h1>Registros de sueño</h1>
        <p class="lead"><h5><a href="/post/create">Generar nuevo registro</a></h5></p>
      </div>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Fecha</th>
            <th>Dormi</th>
            <th>Despertar</th>
            <th>Numero de apneas</th>
            <th>Descripcion</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <?php
            foreach ($posts as $post) :?>
              <?php if ($post->userId == $_SESSION['user_id']): ?>
                <tr>
                <td><?php echo $post->day ?></td>
                <td><?php echo $post->hourInit ?></td>
                <td><?php echo $post->hourFinish ?></td>
                <td><?php echo $post->numApneas ?></td>
                <td><?php echo $post->description ?></td>
                <td>
                  <a href="/post/edit/<?php echo $post->id ?>" class="btn btn-primary">Editar</a>
                </td>
              </tr>
              <?php endif ?>
            <?php endforeach ?>
        </tbody>
      </table>
      <hr>

    </article>

    <?php require "../app/views/parts/footer.php" ?>
    </body>
    <?php require "../app/views/parts/scripts.php" ?>
</html>
