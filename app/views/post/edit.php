<!doctype html>
<html lang="es">
  <head>
    <?php require "../app/views/parts/head.php" ?>
  </head>
  <body>

  <?php require "../app/views/parts/header.php" ?>
<article>
      <p class="h4 mb-4">Edicion del registro del <?php echo "$post->day" ?></p>
      <form action="/post/update" method="post">
        <input type="hidden" name="id" value=" <?php echo $post->id ?> ">
        <input type="hidden" name="userId" value=" <?php echo $post->userId ?> ">

        <div class="form-group">
            <label >Fecha:</label>
            <input type="date" class="form-control" name="day" value="<?php echo $post->day ?>">
          </div>
          <div class="form-group">
            <label >Hora de dormir:</label>
            <input type="time" class="form-control" name="hourInit" value="<?php echo $post->hourInit ?>">
          </div>
          <div class="form-group">
            <label >Hora de despertar:</label>
            <input type="time" class="form-control" name="hourFinish" value="<?php echo $post->hourFinish ?>">
          </div>
          <div class="form-group">
            <label >Numero de apneas:</label>
            <input type="number" class="form-control" name="numApneas" step="0.01" value="<?php echo $post->numApneas ?>">
          </div>
          <div class="form-group">
            <label >Descripcion (opcional)</label>
            <input type="text" class="form-control" name="description" value="<?php echo $post->description ?>">
          </div>
        <div class="form-group">
          <input type="submit" name="submit" class="btn btn-default btn-success" value="Guardar cambios">
          <a href="/post/destroy/<?php echo $post->id ?>" class="btn btn-danger">Eliminar</a>
        </div>
        <div class="form-group">
          <h4><?php echo $_SESSION['validacion'] ?></h4>
          <br><br><br>
        </div>
      </form>
  </article>
  <?php require "../app/views/parts/footer.php" ?>


  </body>
    <?php require "../app/views/parts/scripts.php" ?>
</html>
