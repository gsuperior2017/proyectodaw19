<!doctype html>
<html lang="es">
  <head>
    <?php require "../app/views/parts/head.php" ?>
  </head>
    <body>
      <?php require "../app/views/parts/header.php" ?>
      <article>

        <h1>Registro de sueño</h1>

         <form action="/post/store" method="post">

          <div class="form-group">
            <label >Fecha:</label>
            <input type="date" class="form-control" name="day">
          </div>
          <div class="form-group">
            <label >Hora de dormir:</label>
            <input type="time" class="form-control" name="hourInit" value="00:00">
          </div>
          <div class="form-group">
            <label >Hora de despertar:</label>
            <input type="time" class="form-control" name="hourFinish" value="00:00">
          </div>
          <div class="form-group">
            <label >Numero de apneas:</label>
            <input type="number" class="form-control" name="numApneas" step="0.01">
          </div>
          <div class="form-group">
            <label >Descripcion (opcional)</label>
            <input type="text" class="form-control" name="description">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-default">Guardar sueño</button>
          </div>
        </form>
      </article>
      <br>
    <?php require "../app/views/parts/footer.php" ?>
    </body>
    <?php require "../app/views/parts/scripts.php" ?>
</html>
