<nav>
  <a class="brand" href="/">SGApneas</a>
  <!--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>-->

  <div id="menu">

    <ul>

      <li>
        <a href="/user">
          <?php if ($_SESSION['admin'] == 1): ?>
            Usuarios
          <?php else: ?>
            Mi perfil
          <?php endif ?>
        </a>
      </li>
      <li>
        <a href="/post/check">Historial de sueño</a>
      </li>
      <li class="pos-right">
        <a href="/login/comprobar">
          <?php echo "$_SESSION[usuario]"; ?>
        </a>
      </li>
    </ul>
  </div>
</nav>
