<!doctype html>
<html lang="es">
  <head>
    <?php require "../app/views/parts/head.php" ?>
  </head>
  <body>

<?php require "../app/views/parts/header.php" ?>

  <article>
  <div>
    <h1>Login</h1>
    <form class="form-group" action="/login/login" method="post">
      <label>Correo</label><input type="text" name="email" class="form-control">
      <label>Contraseña</label><input type="password" name="password" class="form-control"><br>
      <input type="submit" name="submit" class="btn btn-default" value="Iniciar sesion">
    </form>
    <a href="/user/create">¿Aun no eres usuario?</a>
    <h4><?php echo "$_SESSION[error]" ?></h4>
  </div>

</article>
<?php require "../app/views/parts/footer.php" ?>


  </body>
    <?php require "../app/views/parts/scripts.php" ?>
</html>
