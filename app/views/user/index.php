<!doctype html>
<html lang="es">
  <head>
    <?php require "../app/views/parts/head.php" ?>
  </head>
    <body>
      <?php require "../app/views/parts/header.php" ?>
      <article>
      <div>
        <h1>Informacion de usuario</h1>
      </div>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Correo</th>
            <th>Fecha de nacimiento</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <?php
            foreach ($users as $user) :?>
              <?php if ($user->id == $_SESSION['user_id'] || $_SESSION['admin'] == 1): ?>
                <tr>
                  <td><?php echo $user->id ?></td>
                  <td><?php echo $user->name ?></td>
                  <td><?php echo $user->surname ?></td>
                  <td><?php echo $user->email ?></td>
                  <td><?php echo $user->birthdate ?></td>
                  <td>
                    <a href="/user/edit/<?php echo $user->id ?>" class="btn btn-primary">Editar</a>
                  </td>
              </tr>

              <?php endif ?>
            <?php endforeach ?>
        </tbody>
      </table>
      <hr>

    </article>

    <?php require "../app/views/parts/footer.php" ?>
    </body>
    <?php require "../app/views/parts/scripts.php" ?>
</html>
