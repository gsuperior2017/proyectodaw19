<!doctype html>
<html lang="es">
  <head>
    <?php require "../app/views/parts/head.php" ?>
  </head>
  <body>

  <?php require "../app/views/parts/header.php" ?>
<article>
  <p class="h4 mb-4">Informacion sobre mi:</p>
    <div class="starter-template">
      <form action="/user/update" method="post">
        <input type="hidden" name="id" value=" <?php echo $user->id ?> ">
        <div class="form-group">
          <label>Nombre*</label>
          <input type="text" name="name" class="form-control" value="<?php echo $user->name ?>">
        </div>
        <div class="form-group">
          <label>Apellidos</label>
          <input type="text" name="surname" class="form-control" value="<?php echo $user->surname ?>">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="email" name="email" class="form-control" value="<?php echo $user->email ?>">
        </div>
        <div class="form-group">
          <label>Contraseña</label>
          <input type="password" name="password" class="form-control" value="<?php echo $user->password ?>">
        </div>
        <div class="form-group">
          <input type="submit" name="submit" class="btn btn-default btn-success" value="Guardar cambios">
          <a href="/user/delete/<?php echo $user->id ?>" class="btn btn-danger">Eliminar</a>
        </div>
        <input type="text" name="error" value="<?php echo $_SESSION['validacion']; ?>" disabled>
      </form>
    </div>

  </main><!-- /.container -->
  </article>
  <?php require "../app/views/parts/footer.php" ?>


  </body>
    <?php require "../app/views/parts/scripts.php" ?>
</html>
