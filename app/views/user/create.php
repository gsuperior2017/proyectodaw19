<!doctype html>
<html lang="es">
  <head>
    <?php require "../app/views/parts/head.php" ?>
  </head>
  <body>

  <?php require "../app/views/parts/header.php" ?>

    <article>
      <div>
        <h1>Alta de usuario</h1>
        <form action="/user/store" method="post">
          <div class="form-group" >
            <label>Nombre*</label><input type="text" name="name" class="form-control">
          </div>
          <div class="form-group" >
            <label>Apellidos</label><input type="text" name="surname" class="form-control">
          </div>
          <div class="form-group" >
            <label>Correo*</label><input type="text" name="email" class="form-control">
          </div>
          <div class="form-group" >
            <label>Fecha de nacimiento</label><input type="date" name="birthdate" class="form-control">
          </div>
          <div class="form-group" >
            <label>Contraseña*</label><input type="password" name="password" class="form-control">
          </div>
          <div class="form-group">
            <input type="submit" name="submit" class="btn btn-default" value="Registrarse">
          </div>
          <h4><?php echo $_SESSION['validacion']; ?></h4>
        </form>

      </div>
    </article>

<?php require "../app/views/parts/footer.php" ?>


  </body>
    <?php require "../app/views/parts/scripts.php" ?>
</html>
