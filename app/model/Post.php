<?php
namespace App\Models;

use PDO;
use Core\Model;

require_once "../core/Model.php";

Class Post extends Model{

    function __construct (){

    }

    public static function all()
    {
        $db = Post::db();
        $statement = $db->query('SELECT * FROM posts ORDER BY day ASC');

        $posts = $statement->fetchAll(PDO::FETCH_CLASS, Post::class);
        //var_dump($posts);
        //exit;

        return $posts;
    }

    public function find($id){
        $db = Post::db();
        $stmt = $db->prepare('SELECT * FROM posts WHERE id LIKE :id');
        $stmt->execute(array(':id'  => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, Post::class);
        $post = $stmt->fetch(PDO::FETCH_CLASS);

        //var_dump($user);
        //exit; lo hace bien

        return $post;
    }

    public function insert(){
        $db = Post::db();
        //var_dump($db);
        $stmt = $db->prepare('INSERT INTO posts(userId, day, hourInit, hourFinish, numApneas, description)
            VALUES(:userId, :day, :hourInit, :hourFinish, :numApneas, :description)');
        $stmt->bindValue(':userId', $this->userId);
        $stmt->bindValue(':day', $this->day);
        $stmt->bindValue(':hourInit', $this->hourInit);
        $stmt->bindValue(':hourFinish', $this->hourFinish);
        $stmt->bindValue(':numApneas', $this->numApneas);
        $stmt->bindValue(':description', $this->description);
        //echo "$this->userId $this->day $this->hourInit $this->hourFinish $this->numApneas $this->description";
        //var_dump($stmt);
        //exit;

        return $stmt->execute();
    }

    public function delete(){
        $db = User::db();
        $stmt = $db->prepare('DELETE FROM posts WHERE id = :id');
        $stmt->bindValue(':id', $this->id);

        //var_dump($stmt);
        return $stmt->execute();
    }

    public function save(){
        $db = User::db();
        $stmt = $db->prepare('UPDATE posts SET day = :day, hourInit = :hourInit, hourFinish = :hourFinish, numApneas = :numApneas, description = :description WHERE id = :id');
        $stmt->bindValue(':day', $this->day);
        $stmt->bindValue(':hourInit' , $this->hourInit);
        $stmt->bindValue(':hourFinish' , $this->hourFinish);
        $stmt->bindValue(':numApneas' , $this->numApneas);
        $stmt->bindValue(':description' , $this->description);
        $stmt->bindValue(':id', $this->id);

        //var_dump($stmt);
        //var_dump($this);
        return $stmt->execute();

    }


}
