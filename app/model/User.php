<?php
//https://rafacabeza.gitbooks.io/dws/content/
namespace App\Models;

use PDO;
use Core\Model;

require_once "../core/Model.php";

Class User extends Model{

    function __construct (){

    }

    public static function all()
    {
        $db = User::db();
        $statement = $db->query('SELECT * FROM users');
        $users = $statement->fetchAll(PDO::FETCH_CLASS, User::class);
        //var_dump($users);
        //echo "$users";
        //exit;
        return $users;
    }

    public function insert (){
        $db = User::db();
        $stmt = $db->prepare('INSERT INTO users(name, surname, email, birthdate, password, active)
            VALUES(:name, :surname, :email, :birthdate, :password, :active)');
        $stmt->bindValue(':name', $this->name);
        $stmt->bindValue(':surname', $this->surname);
        $stmt->bindValue(':email', $this->email);
        $stmt->bindValue(':birthdate', $this->birthdate);
        $stmt->bindValue(':password', $this->password);
        $stmt->bindValue(':active', 1);
        //var_dump($stmt);
        //var_dump($this);

        return $stmt->execute();
    }

    public function find($id){
        $db = User::db();
        $stmt = $db->prepare('SELECT * FROM users WHERE id =:id');
        $stmt->execute(array(':id'  => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, User::class);
        $user = $stmt->fetch(PDO::FETCH_CLASS);

        //var_dump($user);
        //exit; lo hace bien

        return $user;
    }

    public static function comprobar($email){
        $db = User::db();
        $stmt = $db->prepare('SELECT * FROM users WHERE email =:email');
        $stmt->execute(array(':email'  => $email));
        $stmt->setFetchMode(PDO::FETCH_CLASS, User::class);
        $user = $stmt->fetch(PDO::FETCH_CLASS);

        return $user->id;
    }

    public function setPassword($password){
        $password = password_hash($password, PASSWORD_BCRYPT);
        $db = User::db();
        $stmt = $db->prepare('UPDATE users SET password = :password WHERE id = :id');
        $stmt->bindValue(':id', $this->id);
        $stmt->bindValue(':password', $password);

        return $stmt->execute();

    }

    public function passwordVerify($password){
        return password_verify($password, $this->password);
    }

    public function destroy(){
        $db = User::db();
        $postDB = Post::db();
        $stmt = $db->prepare('DELETE FROM users WHERE id = :id');
        $stmt->bindValue(':id', $this->id);
        $deleteAll = $postDB->prepare('DELETE FROM posts WHERE userId = :userId');
        $deleteAll->bindValue(':userId', $this->id);

        $deleteAll->execute();
        return $stmt->execute();
    }

    public function save(){
        $db = User::db();
        $stmt = $db->prepare('UPDATE users SET name = :name, surname = :surname, email = :email, password = :password WHERE id = :id');
        $stmt->bindValue(':name', $this->name);
        $stmt->bindValue(':surname' , $this->surname);
        $stmt->bindValue(':email' , $this->email);
        $stmt->bindValue(':password' , $this->password);
        $stmt->bindValue(':id', $this->id);

        return $stmt->execute();

    }

}//fin de clase User
