<?php

namespace App\Controllers;

require_once '../app/model/User.php';

use \App\Models\User;
/**
*
*/
class HomeController
{

    function __construct()
    {
      if(isset($_SESSION['usuario']) && !empty($_SESSION['usuario']))
      {
        $usuario = $_SESSION['usuario'];
      } else {
        $_SESSION['usuario'] = "Login";
        $_SESSION['productos'] = 0;
      }
    }

    public function index()
    {
        if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
          $user = User::find($_SESSION['user_id']);
        } else {
          $user->name = "Usuario";
          $user->surname = "anonimo";
        }
        require "../app/views/home/index.php";
    }
}
