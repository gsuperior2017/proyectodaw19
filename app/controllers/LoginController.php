<?php

namespace App\Controllers;

require_once '../app/model/User.php';

use \App\Models\User;
/**
*
*/
class LoginController{
    function __construct()
    {
        //echo "Login Controller";
    }

    public function index()
    {
        if(!isset($_SESSION['error']))
            {
                $_SESSION['error'] = "";
            }
        require "../app/views/login/index.php";
    }

    public function comprobar(){
        if ($_SESSION['usuario'] === "Login") {
            header('Location:/login');
        } else {
            header('Location:/login/logout');
        }
    }

    public function login(){
        if(isset($_REQUEST['email']) && !empty($_REQUEST['email']) && isset($_REQUEST['password']) && !empty($_REQUEST['password']))
        {
                $email = $_REQUEST['email'];
                $pwd = $_REQUEST['password'];
                $user = new User();
                $id = User::comprobar($email);
                $user = User::find($id);

                if($user->password == $pwd){
                    $_SESSION['usuario'] = "Log out";
                    //$_SESSION['usuario'] = $user->name;
                    $_SESSION['error'] = "";
                    $_SESSION['user_id'] = $user->id;
                    $_SESSION['admin'] = $user->admin;

                    header('Location:/home');
                } else {
                    $_SESSION['usuario'] = "Login";
                    $_SESSION['error'] = "Error en email y/o contraseña incorrectos";
                    header('Location:/login');
                }
         }else {
            $_SESSION['usuario'] = "Login";
            $_SESSION['error'] = "Error en email y/o contraseña incorrectos";
            header('Location:/login');
        }
    }

    public function logout () {
        $_SESSION['usuario'] = "Login";
        $_SESSION['productos'] = 0;
        unset($_SESSION['cesta'], $_SESSION['admin'], $_SESSION['user_id']);
        header('Location:/login');
    }

}
