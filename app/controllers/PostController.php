<?php

namespace App\Controllers;

require_once '../app/model/Post.php';
require_once '../app/model/User.php';

use \App\Models\Post;
use \App\Models\User;
/**
*
*/
class PostController
{

    function __construct(){

    }

    public function index()
    {
        if ($_SESSION['usuario'] === "Login") {
            header('Location:/login');
        } else {
            $posts = Post::all($_SESSION['user_id']);

            require "../app/views/post/index.php";
        }

    }

    public function check(){
        //echo "entra check";
        //exit;
        if ($_SESSION['usuario'] === "Login") {
            header('Location:/login');
        } else {
            header('Location:/post');
        }
    }

    public function create(){
        //echo "entra create";
        //exit;
        require "../app/views/post/create.php";
    }

    public function store(){
        //echo "Entramos en store";
        $post = new Post();
        //echo "$_SESSION[user_id] $_REQUEST[day] $_REQUEST[hourInit] $_REQUEST[hourFinish] $_REQUEST[numApneas] $_REQUEST[description]";
        $new_date = date('Y-m-d', strtotime($_REQUEST['day']));
        $post->userId = (int) $_SESSION['user_id'];
        $post->day = $new_date;
        $post->hourInit = $_REQUEST['hourInit'];
        $post->hourFinish = $_REQUEST['hourFinish'];
        $post->numApneas = $_REQUEST['numApneas'];
        $post->description = $_REQUEST['description'];
        //var_dump($post);
        //exit;

        $post->insert();

        header("Location:/post");
    }

    public function destroy($arguments) {
        $id = (int) $arguments[0];
        $post = Post::find($id);
        //var_dump($post);
        //exit;
        $post->delete();

        header('Location:/post');
    }

    public function show($arguments){
        $id = (int) $arguments[0];
        $post = Post::find($id);

        require "../app/views/post/show.php";
    }

    public function edit ($arguments){
        $id = (int)$arguments[0];
        $post = Post::find($id);
        //var_dump()
        require "../app/views/post/edit.php";
    }

    public function update () {
        $post = new Post();
        //echo "$_SESSION[user_id] $_REQUEST[day] $_REQUEST[hourInit] $_REQUEST[hourFinish] $_REQUEST[numApneas] $_REQUEST[description]";
        $new_date = date('Y-m-d', strtotime($_REQUEST['day']));
        $post->id = $_REQUEST['id'];
        $post->userId = (int) $_SESSION['user_id'];
        $post->day = $new_date;
        $post->hourInit = $_REQUEST['hourInit'];
        $post->hourFinish = $_REQUEST['hourFinish'];
        $post->numApneas = $_REQUEST['numApneas'];
        $post->description = $_REQUEST['description'];
        $post->save();

        header("Location:/post");
    }

}
