<?php

namespace App\Controllers;

require_once '../app/model/User.php';

use \App\Models\User;
/**
*
*/
class UserController
{

    function __construct()
    {
        if(!isset($_SESSION['validacion']) || empty($_SESSION['validacion']))
        {
            $_SESSION['validacion'] = "*campos obligatorios";
        }
    }

    public function index () {

        if ($_SESSION['usuario'] === "Login") {
            header('Location:/login');
        } else {
            $users = User::all();
            require "../app/views/user/index.php";
        }
    }

    public function show ($arguments)
    {
        $id = (int)$arguments[0];
        $user = User::find($id);
        require "../app/views/user/show.php";
    }

    public function create (){
        require ("../app/views/user/create.php");
    }

    public function store(){

        if (isset($_REQUEST['email']) && isset($_REQUEST['name']) && isset($_REQUEST['password']) && !empty($_REQUEST['email']) && !empty($_REQUEST['name']) && !empty($_REQUEST['password'])) {
            $user = new User();
                $user->name = $_REQUEST['name'];
                $user->surname = $_REQUEST['surname'];
                $user->email = $_REQUEST['email'];
                $user->birthdate = $_REQUEST['birthdate'];
                $user->password = $_REQUEST['password'];
                //var_dump($user); aqui si que entra
                $user->insert();

                header("Location:/user");
        } else {
            $_SESSION['validacion'] = "Rellene los campos obligatorios";
            header("Location:/user/create");
        }


    }

    public function delete($arguments) {
        $id = (int) $arguments[0];
        $user = User::find($id);
        //var_dump($user);
        //exit;
        $user->destroy();

        if ($id == $_SESSION['user_id']) {
            header('Location:/login/comprobar');
        } else {
            header('Location:/user');
        }
    }

    public function edit ($arguments){
        $id = (int)$arguments[0];
        $user = User::find($id);
        require "../app/views/user/edit.php";
    }

    public function update () {
        $id = $_REQUEST['id'];
        //var_dump($_SERVER);
        $user = User::find($id);
        $user->name= $_REQUEST['name'];
        $user->surname= $_REQUEST['surname'];
        $user->age= $_REQUEST['birthdate'];
        $user->email= $_REQUEST['email'];
        $user->save();

        header("Location:/user");
    }


}
