x   CREATE DATABASE IF NOT EXISTS `apneas` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `apneas`;




-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 09-06-2019 a las 19:14:05
-- Versión del servidor: 5.7.24-0ubuntu0.16.04.1
-- Versión de PHP: 7.2.13-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `apneas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `admin` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `birthdate`, `password`, `active`, `admin`) VALUES
(2, 'Alejandro', 'Torrellas Marco', 'alejandro@alejandro.ale', '1997-03-03 00:00:00', 'alejandro', 1, 1),
(10, 'Pablo', 'Garcia Lope', 'pablo@pablo.pab', '2019-06-09 00:00:00', 'secret', 1, 0),
(11, 'Raul', 'Martinez', 'raul@raul.rau', '1980-08-15 00:00:00', 'raul', 1, 0),
(12, 'Stefanie', 'Nadjar', 'stefanie@stefanie.ste', '1965-01-20 00:00:00', 'stefanie', 1, 0),
(13, 'Maria Antonia', 'Chueca Chueca', 'marian@marian.mar', '1970-08-10 00:00:00', 'marian', 1, 0),
(14, 'Gonzalo', 'Torres Catalan', 'gonzalo@gonzalo.gon', '1996-07-31 00:00:00', 'gonzalo', 1, 0),
(16, 'pablo', 'torrellas', 'pmn1930@hotmail.com', '2006-06-09 00:00:00', '123456', 1, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
